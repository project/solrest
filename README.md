# Solrest

## Description

Solrest exposes Rest resources for each Search API Solr Index defined on the site.

### Use cases
If you have a front end application which needs to communicate with Solr directly this module will allow you do so
without passing through Search API. Queries to the Rest endpoint will be passed (more or less) straight to the Solr
server and the data returned as-is.

Complex Search queries can be created using advanced features such as grouping  or JSON facets.
It may be of use to developers searching indices with non-Drupal content.

## Caveats
This service exposes the solr server SELECT endpoint directly. This means all data indexed on that solr instance is
potentially visible wo anyone with access. You should avoid indexing sensitive data if you expose the service to
anonymous users.

Bear in mind the permissions you grant to the rest resource and the data stored in the index.
You may also reduce the allowed parameters passed form the rest resource to solr.

## Instructions

The module will automatically create a Rest resource for every Solr Server defined on the site. These must be enabled,
configured individually and assigned appropriate permissions. The restui module will help you do this.

* Install & enable `restui`
* Enable solrest rest resources via restui `/admin/config/services/rest`
* Grant permissions at `/admin/permissions`
* Review the settings at `/admin/config/services/solrest` to make sure you have enabled the parameters you need

### Querying the resource

Example GET query:

```https://solrest.lndo.site/solrest/my_server_id?_format=json&s=barry```

* The `q` parameter in solr is mapped to `s` to avoid conflicts with Drupal's internal routing.
* Remember drupal requires `_format=json` for JSON REST requests via GET.
* If you receive results, but they aren't quite as expected, check the settings at /admin/config/services/solrest

### Removing rest endpoints

If you delete a search index before disabling the Rest endpoint you will have to remove the rest endpoint manually.
```
drush cdel rest.resource.solrest_resource.name_ofIndex
```

## Alternatives
Search API provides a Rest resource for all it's indices. It will return Drupal rendered cached Drupal entities.

You may also want to create a custom API which accesses Solr directly using Solarium or Search API. 


