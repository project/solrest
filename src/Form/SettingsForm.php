<?php

namespace Drupal\solrest\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure SolRest settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'solrest_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'solrest.settings'
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['solrest_param_whitelist'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Solrest Parameter Whitelist'),
      '#required' => true,
      '#default_value' => $this->config('solrest.settings')->get('solrest_param_whitelist'),
      '#description' => $this->t('A comma separated list of parameter names which will be allowed in the solr query.')
    ];

    $form['solrest_param_regex'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Solrest Parameter Regex whitelist'),
      '#required' => true,
      '#default_value' => $this->config('solrest.settings')->get('solrest_param_regex'),
      '#description' => $this->t('A comma separated list of regex expressions which can be used to whitelist parameters such as <em>f.<fieldname>.facet.sort</em>')
    ];

    $form['solrest_default_rows'] = [
      '#type' => 'number',
      '#title' => $this->t('Solrest Default Rows'),
      '#required' => true,
      '#default_value' => $this->config('solrest.settings')->get('solrest_default_rows') ?? 10,
      '#description' => $this->t('If rows is not specified in a query, this default will be applied.')
    ];

    $form['solrest_max_rows'] = [
      '#type' => 'number',
      '#title' => $this->t('Solrest Maximum Rows'),
      '#required' => true,
      '#default_value' => $this->config('solrest.settings')->get('solrest_max_rows') ?? 20,
      '#description' => $this->t('Maximum number of rows permitted to be requested by consumers. If a higher number of rows is requested this value will take precedence.')
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('solrest.settings')
      ->set('solrest_param_whitelist', $form_state->getValue('solrest_param_whitelist'))
      ->set('solrest_param_regex', $form_state->getValue('solrest_param_regex'))
      ->set('solrest_default_rows', $form_state->getValue('solrest_default_rows'))
      ->set('solrest_max_rows', $form_state->getValue('solrest_max_rows'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
