<?php

namespace Drupal\solrest\Plugin\rest\resource;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\solrest\Exception\SolrestException;
use GuzzleHttp\ClientInterface;
use Solarium\Core\Client\Adapter\Curl;
use Solarium\Core\Client\Adapter\Http;
use Solarium\Client as SolariumClient;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides Solrest Resource.
 *
 * This allows for solr variables to be specified in
 * the rest request to be applied directly to the solr query. Something which
 * is not permitted in search api resources.
 *
 * Serialization class is blank, we do not map input to an entity class
 *
 * @RestResource(
 *   id = "solrest_resource",
 *   label = @Translation("Solrest Resource"),
 *   serialization_class = "",
 *   deriver = "Drupal\solrest\Plugin\Deriver\SolrDeriver",
 *   uri_paths = {
 *     "canonical" = "/solrest/{index_id}",
 *   }
 * )
 */
class SolrestResource extends ResourceBase {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Current Request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected Request $currentRequest;

  /**
   * The event dispatcher.
   *
   * @var \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher
   */
  protected $eventDispatcher;

  /**
   * Guzzle\Client instance.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Symfony\Component\HttpFoundation\Request $current_request
   *   The current request.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function __construct(
    array                    $configuration,
                             $plugin_id,
                             $plugin_definition,
    array                    $serializer_formats,
    LoggerInterface          $logger,
    Request                  $current_request,
    EventDispatcherInterface $eventDispatcher,
    ClientInterface          $http_client,
    ConfigFactoryInterface   $config_factory
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->currentRequest = $current_request;
    $this->eventDispatcher = $eventDispatcher;
    $this->httpClient = $http_client;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('example_rest'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('event_dispatcher'),
      $container->get('http_client'),
      $container->get('config.factory'),
    );
  }

  /**
   * Responds to resource get requests.
   *
   * @return \Drupal\rest\ResourceResponse
   *   Returns Resource response.
   *
   * @throws \Drupal\solrest\SolrestException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\search_api\SearchApiException
   */
  public function get(): ResourceResponse {

    $query_string = $this->currentRequest->getQueryString();

    $request_uri = $this->currentRequest->server->get('REQUEST_URI');
    $query_string = substr($request_uri, strpos($request_uri, "?") + 1);
    $parameters = $this->parseParameters($query_string);

    $data = $this->solrestQuery($parameters);
    $response = new ResourceResponse($data);

    // Limit caching.
    $cache_limit = new CacheableMetadata();
    $cache_limit->setCacheMaxAge(60);
    $response->addCacheableDependency($cache_limit);

    \Drupal::service('page_cache_kill_switch')->trigger();

    return $response;
  }

  /**
   * Queries solr using Search API.
   *
   * @param mixed $params
   *   Arguments.
   *
   * @return mixed
   *   Returns data from solr query.
   *
   * @throws \Drupal\search_api\SearchApiException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\solrest\SolrestException
   */
  protected function solrestQuery($params) {

    $server_id = $this->pluginDefinition["server_id"];
    /** @var \Drupal\search_api\ServerInterface $search_api_index */
    $server = \Drupal::entityTypeManager()
      ->getStorage('search_api_server')
      ->load($server_id);
    /** @var \Drupal\search_api_solr\Plugin\search_api\backend\SearchApiSolrBackend $backend */
    $backend = $server->getBackend();
    /** @var \Drupal\search_api_solr\SolrConnectorInterface $connector */
    $connector = $backend->getSolrConnector();
    $connector_config = $connector->getConfiguration();

    // Manually create client.
    $adapter = extension_loaded('curl') ? new Curl() : new Http();
    $solarium_client = new SolariumClient($adapter, $this->eventDispatcher, $connector_config);
    $solarium_client->createEndpoint($connector_config + ['key' => 'solrest'], TRUE);

    // Make API call to <core>/select.
    $query = $solarium_client->createApi([
      'handler' => $connector_config["core"] . '/select',
      'contenttype' => 'application/json',
      'method' => 'POST',
    ]);

    foreach ($params as $key => $param) {
      $query->addParam($key, $param);
    }

    // PHPS required if we want to control json.nl=map.
    $query->setResponseWriter($query::WT_PHPS);

    try {
      $response = $solarium_client->execute($query);
    } catch (\Exception $e) {
      throw new SolrestException('An error occurred while trying to query solr: ' . $e->getMessage(), $e->getCode(), $e);
    }

    if (200 != $response->getResponse()->getStatusCode()) {
      throw new SolrestException($response->getResponse()
        ->getStatusMessage(), $response->getResponse()->getStatusCode());
    }
    $body = $response->getResponse()->getBody();
    return json_decode($body, TRUE);

  }

  /**
   * Returns solrest settings object.
   *
   * @return array
   */
  protected function getSettings() {
    $settings = $this->configFactory->get('solrest.settings')->get();
    $whitelist = explode(PHP_EOL, $settings['solrest_param_whitelist']);
    $regex = explode(PHP_EOL, $settings['solrest_param_regex']);
    foreach ($whitelist as &$whitelist_item) {
      $whitelist_item = trim($whitelist_item);
    }
    foreach ($regex as &$regex_item) {
      $regex_item = trim($regex_item);
    }
    return array_merge($settings, [
        'solrest_param_whitelist' => $whitelist,
        'solrest_param_regex' => $regex,
      ]
    );
  }

  /**
   * Prepares a list of incoming query parameters for outgoing solr query.
   *
   * We filter allowed parameters and manipulate certain parameters to account
   * for Drupal/PHP limitations.
   *
   * @param mixed $input_parameters
   *   Input parameters from request.
   *
   * @return array
   *   Array of request parameters to send to solr.
   */
  protected function parseParameters($input_parameters) {

    $params = [];

    $settings = $this->getSettings();

    $separated_parameters = explode('&', $input_parameters);

    foreach ($separated_parameters as $param) {

      $p = strpos($param, '=');
      // If no assignment, skip this parameter.
      if (!$p) {
        continue;
      }
      // Decode.
      $key = substr($param, 0, $p);
      $value = substr($param, $p + 1);
      $translated_value = urldecode($value);

      if (!$this->checkParameter($key)) {
        continue;
      }

      if (isset($params[$key])) {
        if (!is_array($params[$key])) {
          $existing_value = $params[$key];
          $params[$key] = [$existing_value];
        }
        $params[$key][] = $translated_value;
      }
      else {
        $params[$key] = $translated_value;
      }
    }

    // $_GET['q'] is reserved for drupal routing, consumers should send as 's'
    // and we convert here before sending to solr. Default to '*'.
    if (isset($params['s'])) {
      $params['q'] = $params['s'];
      unset($params['s']);
    }
    else {
      $params['q'] = '*';
    }

    // Set a default & maximum no of rows.
    if (!isset($params['rows'])) {
      $params['rows'] = $settings['solrest_default_rows'];
    }
    elseif ($params['rows'] > $settings['solrest_max_rows']) {
      $params['rows'] = $settings['solrest_max_rows'];
    }

    // Encode json.facet separately before sending.
    if (isset($params['json.facet']) && is_array($params['json.facet'])) {
      $json = json_encode($params['json.facet']);
      $params['json.facet'] = $json;
    }

    return $params;
  }

  /**
   * Validates parameter against whitelist.
   *
   * @param string $key
   *   Parameter key*.
   *
   * @return bool
   *   TRUE if valid.
   */
  protected function checkParameter(string $key): bool {

    $settings = $this->getSettings();

    if (in_array($key, $settings['solrest_param_whitelist'])) {
      return TRUE;
    }
    else {
      foreach ($settings['solrest_param_regex'] as $regex) {
        preg_match($regex, $key);
        if (preg_match($regex, $key) === 1) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

}
