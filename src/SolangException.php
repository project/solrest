<?php

namespace Drupal\solrest;

use Drupal\search_api\SearchApiException;

/**
 * Represents an exception that occurs in Search API Solr.
 *
 */
class SolrestException extends SearchApiException {}
