<?php

namespace Drupal\solrest\Exception;

use Drupal\search_api_solr\SearchApiSolrException;

/**
 * Represents an exception that occurs in Solrest.
 */
class SolrestException extends SearchApiSolrException {}
